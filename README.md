# Logger

Versatile logging PKG for logging

## Usage

### Create a logger instance

To create a basic logger pass in a context and a span name.

\*_Right now the span name doesn't act like a true span_

```go
ctx, log := logger.New(ctx, "spanName")
```

### Create a logger instance with extra fields

You can pass in extra fields as options that will get logged as details with every log

```go
ctx, log := logger.New(
    ctx,
    "spanName",
    logger.WithField("fieldName", "valueToLog"),
)
```

### Error Log

logs a message prefixed with `Error:`

```go
// basic error log
log.Error(ctx, "this is an error message")

// with an extra field
log.WithField("extraFieldName", "extraFieldValue").Error(ctx, "this is an error message with extra field")

// with error
err := someFunction()
if err != nil {
    log.WithError(err).Error(ctx, "this is an error message with an error attached")
}

// with error and field
err := someFunction()
if err != nil {
    log.WithError(err).
        WithField("extraFieldName", "extraFieldValue").
        Error(ctx, "this is an error message with an error attached")
}
```

### Fatal Log

logs a message prefixed with `Fatal:`

```go
// basic Fatal log
log.Fatal(ctx, "this is a fatal error message")

// with an extra field
log.WithField("extraFieldName", "extraFieldValue").Fatal(ctx, "this is a fatal error message with extra field")

// with error
err := someFunction()
if err != nil {
    log.WithError(err).Fatal(ctx, "this is a fatal error message with an error attached")
}

// with error and field
err := someFunction()
if err != nil {
    log.WithError(err).
        WithField("extraFieldName", "extraFieldValue").
        Fatal(ctx, "this is a fatal error message with an error attached and extra field")
}
```

### Info Log

logs a message prefixed with `Info:`

```go
// basic Info log
log.Info(ctx, "this is an info message")

// with an extra field
log.WithField("extraFieldName", "extraFieldValue").Info(ctx, "this is an info message with extra field")
```

### Warning Log

logs a message prefixed with `Warn:`

```go
// basic Warn log
log.Warn(ctx, "this is a warning message")

// with an extra field
log.WithField("extraFieldName", "extraFieldValue").Warn(ctx, "this is a warning message with extra field")

// with error
err := someFunction()
if err != nil {
    log.WithError(err).Warn(ctx, "this is a warning message with an error attached")
}

// with error and field
err := someFunction()
if err != nil {
    log.WithError(err).
        WithField("extraFieldName", "extraFieldValue").
        Warn(ctx, "this is a warning message with an error attached and extra field")
}
```

## Advanced

There are a few advanced options you can use with your logger.

### Create a logger instance with Slack as an output

Set any number of channels to send any log level to, then every time a log is called it will get sent to the corresponding channels in slack.

```go
ctx, log := logger.New(
    ctx,
    "spanName",
    logger.WithSlack(
        os.Getenv("SLACK_BOT_TOKEN"),
        logger.Groups{
            logger.ERROR:   []string{"slackErrorChannelID"},
            logger.FATAL:   []string{"slackFatalChannelID"},
            logger.INFO:    []string{"slackInfoChannelID"},
            logger.WARNING: []string{"slackWarningChannelID"},
        },
    ),
)
```

### Create a logger instance that sends requests to webhook endpoints

Set any number of endpoints to send your logs to, then every time a log is called it will make a request to the all the endpoints defined

```go
ctx, log := logger.New(
    ctx,
    "spanName",
    logger.WithWebhook(
        "message", // the key used for the message in the JSON payload ie: {"message": "some log meessge here"}
        logger.Groups{
            logger.ERROR:   []string{"https://test.com/error"},
            logger.FATAL:   []string{"https://test.com/fatal"},
            logger.INFO:    []string{"https://test.com/info"},
            logger.WARNING: []string{"https://test.com/warning"},
        },
    ),
)
```

#### Request Payload

Request body that gets sent as a POST

```json
{
  "message": "log message:\n{\"fieldName\": \"valueToLog\"}"
}
```
