module gitlab.com/5pm.dev/go/logger

go 1.20

require (
	github.com/google/uuid v1.6.0
	github.com/slack-go/slack v0.12.2
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
)
