package logger

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/google/uuid"
)

// @TODO: work out a better way to keep track of trace orders for full run history
// @TODO: add trace information to the log also

// important fields used for the logger
type LogKey string

const (
	errorField LogKey = "error"
	logFields  LogKey = "logFields" // the field in the context were the list of current fields is stored
	requestID  LogKey = "requestID" // request id added to the context when the request is processed
	spanID     LogKey = "spanID"    // an id generated with each new logger, differentiates between two fo the same function calls in the same context
	spanName   LogKey = "spanName"  // the name passed in when creating the new logger

	slackKey    LogKey = "slackKey"
	slackGroups LogKey = "slackGroups"

	webhookKey    LogKey = "webhookKey"
	webhookGroups LogKey = "webhookGroups"
)

// Log type constants
type LogType int

const (
	INFO    LogType = 1
	WARNING LogType = 2
	ERROR   LogType = 3
	FATAL   LogType = 4
)

var logTypePrefix = map[LogType]string{
	INFO:    "Info",
	WARNING: "Warning",
	ERROR:   "Error",
	FATAL:   "Fatal",
}

// used to map log types to external channel sets
type Groups map[LogType][]string

// Fields set of key value pairs that are added to the context as 1 field each
type Fields map[LogKey]interface{}

// loggerFields list of fields to be logged that have been added to the context
type loggerFields map[LogKey]bool

// Logger
type Logger struct {
	err    error
	fields Fields
	logger *log.Logger
}

// New creates a new instance of a logger
func New(ctx context.Context, span string, options ...LogOption) (context.Context, *Logger) {
	var logger Logger

	// set some default values
	ctx = context.WithValue(ctx, spanName, span)
	ctx = context.WithValue(ctx, spanID, uuid.New().String())

	logger.logger = log.New(os.Stdout, "", log.Ldate)

	// loop through the options an execute them
	for _, option := range options {
		ctx = option(ctx)
	}

	return ctx, &logger
}

// WithError adds an error to be logged to the logger
func (l *Logger) WithError(err error) *Logger {
	l.err = err
	return l
}

// WithField adds a single field to the logger
func (l *Logger) WithField(key string, value interface{}) *Logger {
	if l.fields == nil {
		l.fields = make(Fields)
	}

	l.fields[LogKey(key)] = value
	return l
}

// WithFields adds multiple fields to the logger
func (l *Logger) WithFields(fields Fields) *Logger {
	if l.fields == nil {
		l.fields = fields
		return l
	}

	// add all the fields
	for key, value := range fields {
		l.fields[key] = value
	}

	return l
}

// Error logs an error line along with any fields that have been added to the context or logger
func (l *Logger) Error(ctx context.Context, msg string) {
	l.log(ctx, ERROR, msg)
}

// Fatal logs a fatal line along with any fields that have been added to the context or logger
func (l *Logger) Fatal(ctx context.Context, msg string) {
	l.log(ctx, FATAL, msg)
}

// Info logs a basic info line along with any fields that have been added to the context or logger
func (l *Logger) Info(ctx context.Context, msg string) {
	l.log(ctx, INFO, msg)
}

// Warn logs a warning line along with any fields that have been added to the context or logger
func (l *Logger) Warn(ctx context.Context, msg string) {
	l.log(ctx, WARNING, msg)
}

// log handles sending the actual log message out
func (l *Logger) log(ctx context.Context, logType LogType, msg string) {
	details := l.message(ctx, false)

	l.logger.SetPrefix(fmt.Sprintf("%s: ", logTypePrefix[logType]))
	l.logger.Printf("%s: %s\n", msg, details)

	var wg sync.WaitGroup

	// send to slack if the key is set
	if key := ctx.Value(slackKey); key != nil {
		wg.Add(1)
		go func() {
			defer wg.Done()
			l.slackWrite(ctx, logType, msg)
		}()
	}

	// send to webhook if the key is set
	if key := ctx.Value(webhookKey); key != nil {
		wg.Add(1)
		go func() {
			defer wg.Done()
			l.webhookWrite(ctx, logType, msg)
		}()
	}

	wg.Wait()
}

// message uses the logger and the context to build a json message body that will get sent with the log message
func (l *Logger) message(ctx context.Context, pretty bool) string {
	body := make(map[LogKey]interface{})

	// set the default fields
	body[spanID] = ctx.Value(spanID)
	body[spanName] = ctx.Value(spanName)

	if reqID := ctx.Value(requestID); reqID != nil {
		body[requestID] = reqID
	}

	// add the error if its set
	if l.err != nil {
		body[errorField] = l.err.Error()
	}

	// add the fields from the context
	ctxFields := ctx.Value(logFields)
	if ctxFields != nil {
		for field := range ctxFields.(loggerFields) {
			if value := ctx.Value(field); value != nil {
				body[field] = value
			}
		}
	}

	// add the fields added directly to the logger, these supersede fields in context
	if len(l.fields) > 0 {
		for key, value := range l.fields {
			body[key] = value
		}
	}

	// return the pretty message if its requested
	if pretty {
		if msg, _ := json.MarshalIndent(body, "", "    "); len(msg) > 0 {
			return string(msg)
		}
	}

	// return the message details if its not empty
	if msg, _ := json.Marshal(body); len(msg) > 0 {
		return string(msg)
	}

	return ""
}
