package logger

import (
	"context"
	"errors"
	"testing"
)

func TestInfo(t *testing.T) {
	ctx, log := New(context.Background(), "testInfo")
	log.Info(ctx, "Some test message")
}

func TestWithError(t *testing.T) {
	_, log := New(context.Background(), "testWithError")
	log = log.WithError(errors.New("some error"))

	if log.err == nil {
		t.Error("no error was added to the logger")
	}
}

func TestWithField(t *testing.T) {
	_, log := New(context.Background(), "testWithField")
	log = log.WithField("field", "value")

	if len(log.fields) == 0 {
		t.Error("no fields were added to the logger")
	}
}

func TestWithFields(t *testing.T) {
	_, log := New(context.Background(), "testWithFields")
	log = log.WithFields(Fields{"field": "value"})

	if len(log.fields) == 0 {
		t.Error("no fields were added to the logger")
	}
}
