package logger

import (
	"context"
)

// LogOption option functions used to add new fields or options to the logger
type LogOption func(context.Context) context.Context

// WithField adds a new field to the logger
func WithField(key string, value interface{}) LogOption {
	return func(ctx context.Context) context.Context {
		ctx = addField(ctx, LogKey(key), value, true)
		return ctx
	}
}

// WithFields adds multiple new fields to the logger
func WithFields(fields Fields) LogOption {
	return func(ctx context.Context) context.Context {
		for key, value := range fields {
			ctx = addField(ctx, LogKey(key), value, true)
		}

		return ctx
	}
}

// addField adds a new field to the context
func addField(ctx context.Context, key LogKey, value interface{}, logField bool) context.Context {
	ctx = context.WithValue(ctx, key, value)

	// if the field should be logged add it to the map
	if logField {
		// create log fields if the map doesn't exist yet
		f, ok := ctx.Value(logFields).(loggerFields)
		if f == nil || !ok {
			f = make(loggerFields)
			ctx = context.WithValue(ctx, logFields, f)
		}

		// add the new field to the map
		if _, found := f[key]; !found {
			f[key] = true
		}
	}

	return ctx
}
