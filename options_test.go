package logger

import (
	"context"
	"testing"
)

func TestAddField(t *testing.T) {
	ctx := addField(context.Background(), "test", "testValue", true)
	v := ctx.Value(LogKey("test"))
	if v == nil {
		t.Errorf("value for (%s) is empty", "test")
	}

	if s, ok := v.(string); !ok || s != "testValue" {
		t.Error("value is wrong")
	}
}
