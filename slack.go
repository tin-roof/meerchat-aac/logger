package logger

import (
	"context"
	"fmt"
	"log"

	"github.com/slack-go/slack"
)

// WithSlack logger option adds the Slack details to the logger
func WithSlack(key string, groups Groups) LogOption {
	return func(ctx context.Context) context.Context {
		ctx = addField(ctx, slackKey, key, false)
		ctx = addField(ctx, slackGroups, groups, false)
		return ctx
	}
}

// slackWrite sends the message to all the groups that have been added
func (l Logger) slackWrite(ctx context.Context, logType LogType, msg string) {

	// get details needed from context
	key := ctx.Value(slackKey)
	groups := ctx.Value(slackGroups).(Groups)

	// if there are no groups just return
	if groups == nil {
		log.Printf("(error) there no destination groups setup for Slack")
		return
	}

	// build a pretty layout of the json details
	details := l.message(ctx, true)

	// create the Slack client
	client := slack.New(key.(string))

	// send the message to each group in the type
	for _, slackGroup := range groups[logType] {
		_, _, err := client.PostMessage(slackGroup, buildSlackMSG(logType, msg, details)...)
		if err != nil {
			log.Printf("(error) there was an error sending log to Slack: %s", err)
			return
		}
	}
}

// buildSlackMSG builds the block structure for the message to be sent to Slack
func buildSlackMSG(logType LogType, msg, details string) (options []slack.MsgOption) {

	// add the appropriate emoji to the message
	switch logType {
	case ERROR:
		msg = ":no_entry_sign: " + msg
	case FATAL:
		msg = ":skull: " + msg
	case INFO:
		msg = ":spiral_note_pad: " + msg
	case WARNING:
		msg = ":warning: " + msg
	}

	// build the message in blocks
	blocks := []slack.Block{
		&slack.SectionBlock{
			Type: slack.MBTSection,
			Text: &slack.TextBlockObject{
				Type: "mrkdwn",
				Text: msg,
			},
		},
		&slack.DividerBlock{
			Type: slack.MBTDivider,
		},
		&slack.ContextBlock{
			Type: slack.MBTContext,
			ContextElements: slack.ContextElements{
				Elements: []slack.MixedElement{
					&slack.TextBlockObject{
						Type: "mrkdwn",
						Text: "Log Details",
					},
				},
			},
		},
		&slack.ContextBlock{
			Type: slack.MBTContext,
			ContextElements: slack.ContextElements{
				Elements: []slack.MixedElement{
					&slack.TextBlockObject{
						Type: "mrkdwn",
						Text: fmt.Sprintf("```%s```", details),
					},
				},
			},
		},
	}

	options = append(options, slack.MsgOptionBlocks(blocks...))

	return options
}
