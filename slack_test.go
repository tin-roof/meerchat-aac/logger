package logger

import (
	"context"
	"testing"
)

func TestWithSlack(t *testing.T) {
	ctx, _ := New(
		context.Background(),
		"testWithSlack",
		WithSlack(
			"test-slack-key",
			Groups{
				INFO:  []string{"test-channel"},
				ERROR: []string{"test-channel"},
			},
		),
	)

	if key := ctx.Value(slackKey); key == nil {
		t.Error("no Slack key was added to the context")
	}

	if groups := ctx.Value(slackGroups).(Groups); groups == nil {
		t.Error("no Slack groups were added to the context")
	} else {
		if len(groups) != 2 {
			t.Error("there should be 2 Slack groups added to the context")
		}
	}
}
