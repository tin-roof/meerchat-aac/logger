package logger

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

// WithWebhook logger option adds the webhook details to the logger
func WithWebhook(msgKey string, groups Groups) LogOption {
	return func(ctx context.Context) context.Context {
		ctx = addField(ctx, webhookKey, msgKey, false)
		ctx = addField(ctx, webhookGroups, groups, false)
		return ctx
	}
}

// webhookWrite send the message to the webhooks
func (l Logger) webhookWrite(ctx context.Context, logType LogType, msg string) {

	// get details needed from context
	groups := ctx.Value(webhookGroups).(Groups)

	// if there are no groups just return
	if groups == nil {
		log.Printf("(error) there no destination groups setup for webhooks")
		return
	}

	// get the message key that is used for the json payload
	msgKey := ctx.Value(webhookKey).(string)
	if msgKey == "" {
		msgKey = "message"
	}

	// build a pretty layout of the json details
	msg = fmt.Sprintf("%s\n%s", msg, l.message(ctx, true))

	// escape the message to make sure any json in the message doesn't cause issues with the full body
	escapedMsg, err := json.Marshal(msg)
	if err != nil {
		log.Printf("(error) failed to escape message: %v", err)
		return
	}

	// create the http client to make the requests
	client := &http.Client{}

	// loop over the webhooks and send the message
	for _, webhook := range groups[logType] {
		req, err := http.NewRequest(
			http.MethodPost,
			webhook,
			bytes.NewBuffer([]byte(`{"`+msgKey+`":`+string(escapedMsg)+`}`)),
		)
		if err != nil {
			log.Printf("(error) there was an error building webhook request: %s", err)
			continue
		}

		req.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			log.Printf("(error) there was an error sending log to webhook: %s", err)
			continue
		}
		defer resp.Body.Close()

		// read the body
		body, _ := io.ReadAll(resp.Body)
		if b := string(body); b != "ok" && b != "" {
			log.Printf("(error) something went wrong while sending message to webhook (%s): %s", webhook, string(body))
		}
	}
}
