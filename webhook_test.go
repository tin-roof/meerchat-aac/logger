package logger

import (
	"context"
	"testing"
)

func TestWithWebhook(t *testing.T) {
	ctx, _ := New(
		context.Background(),
		"testWithWebhook",
		WithWebhook(
			"message",
			Groups{
				INFO:  []string{"https://hooks.logger.test/info"},
				ERROR: []string{"https://hooks.logger.test/error"},
			},
		),
	)

	if key := ctx.Value(webhookKey); key == nil {
		t.Error("no webhook key was added to the context")
	}

	if groups := ctx.Value(webhookGroups).(Groups); groups == nil {
		t.Error("no webhook groups were added to the context")
	} else {
		if len(groups) != 2 {
			t.Error("there should be 2 webhook groups added to the context")
		}
	}

}
